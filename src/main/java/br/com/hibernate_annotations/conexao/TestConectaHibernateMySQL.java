package br.com.hibernate_annotations.conexao;

import org.hibernate.Session;

public class TestConectaHibernateMySQL {
	
	public static void main(String[] args) {
		Session sessao = null;
		try {
			sessao = HibernateUtil.getSessionFactory().openSession();
			System.out.println("Conectou!");
		} finally {
			sessao.close();
		}
	}

}
